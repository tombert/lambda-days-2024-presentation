NUM_TEETH = 10; 
PI = 3.1415926;

module gear(num_teeth, d) {
    difference() {
    cylinder(h=10, d = d, center = true);

    for (i = [1:1:num_teeth]){
      
        rotate([0,0,i * (360 / num_teeth)])
              translate([0,d/2, 0]) {
                cube([(3.141592 * d) / (num_teeth*2) , 10, 100], center = true);
            }
        }
    }
    }
    
 gear(10, 40);
    
 
